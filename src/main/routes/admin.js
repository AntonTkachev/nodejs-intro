const express = require('express');
const checker = require("../middleware/checker");
const router = express.Router();

router.get("/", checker, function (req, res) {
    console.log('Admin login: ', Date.now());
    res.sendStatus(200)

});
router.get("/:id", checker, function (req, res) {
    console.log('Admin with id: ' + req.params.id + ' login: ', Date.now());
    res.sendStatus(200)
});

module.exports = router;