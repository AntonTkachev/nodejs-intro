const HEADER_AUTHORIZATION = 'authorization';

const expectedPrefix = 'MyToken';

function check(req, res, next) {
    const authorization = req.headers[HEADER_AUTHORIZATION];

    function validationToken(token) {
        return true;
    }

    if (authorization) {
        const arr = authorization.split(' ');
        const [prefix, token] = arr;
        if (prefix === expectedPrefix) {
            if (validationToken(token)) res.sendStatus(200);
            else res.status(401).send(`Your token: '${token}' unvalidated`)
        } else res.status(401).send(`Your prefix: '${prefix}' not equals expected`)

    } else res.sendStatus(401);
}

module.exports = check;