const express = require('express');
const app = express();

const users = require('./routes/users');
const admin = require('./routes/admin');

app.get('/', function (req, res) {
    res.send('Hello World');
    res.sendStatus(200)
});

app.listen(3000);

app.use('/users', users);
app.use('/admin', admin);